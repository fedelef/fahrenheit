// Fernando Fedele

package sheridan;

public class Fahrenheit {

		public static int fromCelsius(int degreesCelsius) throws NumberFormatException {
			if (degreesCelsius < 0) {
				throw new NumberFormatException();
			}
			double fahrenheit = (((double)degreesCelsius) * 9 / 5) + 32;
			return (int) Math.round(fahrenheit); 
		}
}
