// Fernando Fedele

package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {

	@Test
	public void testFromCelsiusRegular() {
		int degreesCelsius = 25;
		assertTrue("Temp is incorrect", Fahrenheit.fromCelsius(degreesCelsius) == 77);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testFromCelsiusException() {
		int degreesCelsius = -25;
		Fahrenheit.fromCelsius(degreesCelsius);
		fail("Exception was not thrown");
	}
	
	@Test
	public void testFromCelsiusBoundaryIn() {
		int degreesCelsius = 1; //33.8 rounded up to 34
		assertTrue("Temp is incorrect", Fahrenheit.fromCelsius(degreesCelsius) == 34); 
	}
	
	@Test (expected=NumberFormatException.class)
	public void testFromCelsiusBoundaryOut() {
		int degreesCelsius = -1;
		Fahrenheit.fromCelsius(degreesCelsius);
		fail("Exception was not thrown");
	}

}
